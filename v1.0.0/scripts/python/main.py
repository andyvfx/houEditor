# import sys
# sys.path.insert(0, r"D:\fxTools\CGRnDStudio\PublicLib\PyQt\Lib\site-packages")

# print(sys.version)

# from PyQt4.QtCore import *
# from PyQt4.QtGui import *

# class MyMainWindow(QMainWindow):

#     def __init__(self, parent=None):

#         super(MyMainWindow, self).__init__(parent)
#         self.form_widget = FormWidget(self) 
#         self.setCentralWidget(self.form_widget) 


# class FormWidget(QWidget):

#     def __init__(self, parent):        
#         super(FormWidget, self).__init__(parent)
#         self.layout = QVBoxLayout(self)

#         self.button1 = QPushButton("Button 1")
#         self.layout.addWidget(self.button1)

#         self.button2 = QPushButton("Button 2")
#         self.layout.addWidget(self.button2)

#         self.frame = QFrame()
#         self.frame.setFrameShape(QFrame.StyledPanel)
#         self.frame.setFrameShadow(QFrame.Sunken)
#         self.layout.addWidget(self.frame)

#         self.setLayout(self.layout)

# class GameWindow(QMainWindow):
#     def __init__(self, parent=None):
#         QMainWindow.__init__(self, parent)

#         self.initUI()

#     def initUI(self):
#         self.setGeometry(300, 300, 1280, 800)
#         self.setWindowTitle("Intel")

#         self.centralwidget = QWidget(self)
#         self.frame = QFrame(self)
#         self.frame.setFrameShape(QFrame.VLine)
#         self.frame.setLineWidth(16)
#         self.frame.setFrameShadow(QFrame.Sunken)
#         self.frame.resize(300, 300)
#         self.frame.setStyleSheet("background-color: rgb(200, 255, 255)")
#         self.centralwidget.setCentralWidget(self.frame)



# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = MyMainWindow()
#     window.show()
#     sys.exit(app.exec_())

# from PyQt4.QtCore import QRegExp
# from PyQt4.QtGui import QColor, QTextCharFormat, QFont, QSyntaxHighlighter


# def format(color, style=''):
#     """Return a QTextCharFormat with the given attributes.
#     """
#     _color = QColor()
#     _color.setNamedColor(color)

#     _format = QTextCharFormat()
#     _format.setForeground(_color)
#     if 'bold' in style:
#         _format.setFontWeight(QFont.Bold)
#     if 'italic' in style:
#         _format.setFontItalic(True)

#     return _format


# # Syntax styles that can be shared by all languages
# STYLES = {
#     'keyword': format('blue'),
#     'operator': format('red'),
#     'brace': format('darkGray'),
#     'defclass': format('black', 'bold'),
#     'string': format('magenta'),
#     'string2': format('darkMagenta'),
#     'comment': format('darkGreen', 'italic'),
#     'self': format('black', 'italic'),
#     'numbers': format('brown'),
# }


# class PythonHighlighter (QSyntaxHighlighter):
#     """Syntax highlighter for the Python language.
#     """
#     # Python keywords
#     keywords = [
#         'and', 'assert', 'break', 'class', 'continue', 'def',
#         'del', 'elif', 'else', 'except', 'exec', 'finally',
#         'for', 'from', 'global', 'if', 'import', 'in',
#         'is', 'lambda', 'not', 'or', 'pass', 'print',
#         'raise', 'return', 'try', 'while', 'yield',
#         'None', 'True', 'False',
#     ]

#     # Python operators
#     operators = [
#         '=',
#         # Comparison
#         '==', '!=', '<', '<=', '>', '>=',
#         # Arithmetic
#         '\+', '-', '\*', '/', '//', '\%', '\*\*',
#         # In-place
#         '\+=', '-=', '\*=', '/=', '\%=',
#         # Bitwise
#         '\^', '\|', '\&', '\~', '>>', '<<',
#     ]

#     # Python braces
#     braces = [
#         '\{', '\}', '\(', '\)', '\[', '\]',
#     ]

#     def __init__(self, document):
#         QSyntaxHighlighter.__init__(self, document)

#         # Multi-line strings (expression, flag, style)
#         # FIXME: The triple-quotes in these two lines will mess up the
#         # syntax highlighting from this point onward
#         self.tri_single = (QRegExp("'''"), 1, STYLES['string2'])
#         self.tri_double = (QRegExp('"""'), 2, STYLES['string2'])

#         rules = []

#         # Keyword, operator, and brace rules
#         rules += [(r'\b%s\b' % w, 0, STYLES['keyword'])
#             for w in PythonHighlighter.keywords]
#         rules += [(r'%s' % o, 0, STYLES['operator'])
#             for o in PythonHighlighter.operators]
#         rules += [(r'%s' % b, 0, STYLES['brace'])
#             for b in PythonHighlighter.braces]

#         # All other rules
#         rules += [
#             # 'self'
#             (r'\bself\b', 0, STYLES['self']),

#             # Double-quoted string, possibly containing escape sequences
#             (r'"[^"\\]*(\\.[^"\\]*)*"', 0, STYLES['string']),
#             # Single-quoted string, possibly containing escape sequences
#             (r"'[^'\\]*(\\.[^'\\]*)*'", 0, STYLES['string']),

#             # 'def' followed by an identifier
#             (r'\bdef\b\s*(\w+)', 1, STYLES['defclass']),
#             # 'class' followed by an identifier
#             (r'\bclass\b\s*(\w+)', 1, STYLES['defclass']),

#             # From '#' until a newline
#             (r'#[^\n]*', 0, STYLES['comment']),

#             # Numeric literals
#             (r'\b[+-]?[0-9]+[lL]?\b', 0, STYLES['numbers']),
#             (r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b', 0, STYLES['numbers']),
#             (r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b', 0, STYLES['numbers']),
#         ]

#         # Build a QRegExp for each pattern
#         self.rules = [(QRegExp(pat), index, fmt)
#             for (pat, index, fmt) in rules]

#     def highlightBlock(self, text):
#         """Apply syntax highlighting to the given block of text.
#         """
#         # Do other syntax formatting
#         for expression, nth, format in self.rules:
#             index = expression.indexIn(text, 0)

#             while index >= 0:
#                 # We actually want the index of the nth match
#                 index = expression.pos(nth)
#                 length = expression.cap(nth).length()
#                 self.setFormat(index, length, format)
#                 index = expression.indexIn(text, index + length)

#         self.setCurrentBlockState(0)

#         # Do multi-line strings
#         in_multiline = self.match_multiline(text, *self.tri_single)
#         if not in_multiline:
#             in_multiline = self.match_multiline(text, *self.tri_double)

#     def match_multiline(self, text, delimiter, in_state, style):
#         """Do highlighting of multi-line strings. ``delimiter`` should be a
#         ``QRegExp`` for triple-single-quotes or triple-double-quotes, and
#         ``in_state`` should be a unique integer to represent the corresponding
#         state changes when inside those strings. Returns True if we're still
#         inside a multi-line string when this function is finished.
#         """
#         # If inside triple-single quotes, start at 0
#         if self.previousBlockState() == in_state:
#             start = 0
#             add = 0
#         # Otherwise, look for the delimiter on this line
#         else:
#             start = delimiter.indexIn(text)
#             # Move past this match
#             add = delimiter.matchedLength()

#         # As long as there's a delimiter match on this line...
#         while start >= 0:
#             # Look for the ending delimiter
#             end = delimiter.indexIn(text, start + add)
#             # Ending delimiter on this line?
#             if end >= add:
#                 length = end - start + add + delimiter.matchedLength()
#                 self.setCurrentBlockState(0)
#             # No; multi-line string
#             else:
#                 self.setCurrentBlockState(in_state)
#                 length = text.length() - start + add
#             # Apply formatting
#             self.setFormat(start, length, style)
#             # Look for the next match
#             start = delimiter.indexIn(text, start + length)

#         # Return True if still inside a multi-line string, False otherwise
#         if self.currentBlockState() == in_state:
#             return True
#         else:
#             return False


# d1 = {"a": 1, "b": 2, "c": 3}
# d2 = {"a": 1,
#       "b": 2,
#       "c": 3,
# }


# print(d1, d2)

# import sys
# sys.path.insert(0, r"F:\Python\test\Lib\site-packages")
# import PyQt4
# print(dir(PyQt4))
# from PyQt4 import QtGui
# print(QtGui.__file__)
# print(PyQt4.__file__)

# import sys

# from PyQt4.QtGui import *
# from PyQt4.QtCore import *

# class HistoryWidget(QTextBrowser):
#     def __init__(self):
#         super(HistoryWidget, self).__init__()
#         # Auto wrap
#         self.setWordWrapMode(QTextOption.NoWrap)
#         self.fontSize = 16

#     def showMessage(self, msg):
#         self.moveCursor(QTextCursor.End)
#         cursor = self.textCursor()
#         cursor.insertText(str(msg)+"\n")
#         self.setTextCursor(cursor)
#         self.moveCursor(QTextCursor.End)
#         self.ensureCursorVisible()

#     def wheelEvent(self, event):
#         if event.modifiers() == Qt.ControlModifier:
#             if event.delta() > 0:
#                 self.changeFontSize(True)
#             else:
#                 self.changeFontSize(False)
#         QTextBrowser.wheelEvent(self, event)

#     def changeFontSize(self, up):
#         if up:
#             self.fontSize = self.fontSize + 1
#         else:
#             self.fontSize = max(1, self.fontSize - 1)
#         self.setTextEditFontSize(self.fontSize)

#     def setTextEditFontSize(self, size):
#         style = '''QTextEdit
#         {
#             font-size: %spx;
#         }''' % size
#         self.setStyleSheet(style)

# class MainWindow(QMainWindow):
#     def __init__(self):
#         QMainWindow.__init__(self)
#         self.resize(350, 250)

#         history = HistoryWidget()
#         self.setCentralWidget(history)
#         history.showMessage("import hou")
#         history.showMessage("hou.selectedNodes()")

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = MainWindow()
#     window.show()
#     sys.exit(app.exec_())


# class Example(QtGui.QWidget):

#     def __init__(self):
#         super(Example, self).__init__()
#         self.initUI()

#     def initUI(self):
#         self.btn = QtGui.QPushButton('Dialog', self)
#         self.btn.move(20, 20)
#         self.btn.clicked.connect(self.showDialog)
#         self.le = QtGui.QLineEdit(self)
#         self.le.move(130, 22)
#         self.setGeometry(300, 300, 290, 150)
#         self.setWindowTitle('Input dialog')
#         self.show()

#     def showDialog(self):
#         text, ok = QtGui.QInputDialog.getText(self, 'Input Dialog', 'Enter your name:')
#         if ok:
#             self.le.setText(str(text))
# ex = Example()



# @echo off
# set MAYAPYQTBUILD=%~dp0
# set MAYAPYQTBUILD=%MAYAPYQTBUILD:~0,-1%
# if exist v:\nul subst v: /d
# subst v: "%MAYAPYQTBUILD%"
# set SIPDIR=v:\sip-4.16.7
# set MSVC_DIR=C:\Program Files (x86)\Microsoft Visual Studio 14.0
# if ["%LIBPATH%"]==[""] call "%MSVC_DIR%\VC\vcvarsall" amd64
# set MAYA_LOCATION=C:\Program Files\Autodesk\Maya2016

# set INCLUDE=%INCLUDE%;%MAYA_LOCATION%\include\python2.7
# set LIB=%LIB%;%MAYA_LOCATION%\lib

# pushd %SIPDIR%
# "%MAYA_LOCATION%\bin\mayapy" configure.py
# nmake
# nmake install
# popd

# pause

# import maya.cmds as cmds
# import maya.OpenMayaUI as omui
# from PySide import QtCore, QtGui
# from shiboken import wrapInstance

# def _get_maya_main_window():
#     pointer = omui.MQtUtil.mainWindow()
#     return wrapInstance(long(pointer), QtGui.QWidget)

# print(_get_maya_main_window())

# class TestDialog(QtGui.QDialog):
#     def __init__(self):
#         super(TestDialog, self).__init__(_get_maya_main_window())
#         self._initUI()
    
#     def _initUI(self):
#         pass

# if __name__ == "__main__":
#     win = TestDialog()
#     win.show()

import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle(self.tr("MainWindow"))
        self.setMinimumSize(400, 300)

        # Central widget
        widget = QTextEdit()
        self.items = QDockWidget("Dockable", self)
        self.items.setWidget(widget)
        self.items.setFloating(False)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.items)

        # QAction
        self.actionDo = QAction("Do", self)
        self.actionDo.setShortcut("Ctrl+Q")
        self.actionDo.setStatusTip(self.trUtf8("Do something."))
        self.saveAction = QAction(QIcon(r"C:\Python27\Lib\site-packages\PyQt4\examples\demos\textedit\images\win\filesave.png"), "&Save...", self)
        self.saveAction.setShortcut("Ctrl+S")
        self.saveAction.setStatusTip("Save the current file")

        # QStatusBar
        self.setStatusBar(QStatusBar())

        # Menu
        menu = self.menuBar()
        menuAction = menu.addMenu("&Action")
        menuAction.addAction(self.actionDo)

        # Tool Bar
        self.fileToolBar = self.addToolBar("File")
        self.fileToolBar.addAction(self.saveAction)

if __name__ == "__main__":
    app = QApplication([])
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())