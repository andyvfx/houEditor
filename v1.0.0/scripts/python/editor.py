import sys
sys.path.insert(0, r"D:\fxTools\CGRnDStudio\PublicLib\PyQt\Lib\site-packages")
from PyQt4 import QtGui
import syntax

app = QtGui.QApplication([])
editor = QtGui.QPlainTextEdit()
highlight = syntax.PythonHighlighter(editor.document())
editor.show()

# Load syntax.py into the editor for demo purposes
infile = open(r'F:\houEditor\v1.0.0\python2.7libs\syntax.py', 'r')
editor.setPlainText(infile.read())

app.exec_()
