try:
    from PySide2.QtWidgets import *
    from PySide2.QtGui import *
    from PySide2.QtCore import *
except ImportError:
    from PySide.QtGui import *
    from PySide.QtCore import *

from .inputWidget import InputWidget


class Container(QWidget):
    def __init__(self, text, parent, desk):
        super(Container, self).__init__()
        hBox = QHBoxLayout(self)
        hBox.setSpacing(0)
        hBox.setContentsMargins(0, 0, 0, 0)
        self.edit = InputWidget(parent, desk)
        hBox.addWidget(self.edit)


class TabWidget(QTabWidget):
    def __init__(self, parent=None):
        super(TabWidget, self).__init__(parent)
        self.parent = parent
        self.setTabsClosable(True)
        self.setMovable(True)
        self.tabBar().setContextMenuPolicy(Qt.CustomContextMenu)

        self.desk = QApplication.desktop()

        # self.currentChanged.connect(self.hide)

    def addNewTab(self, name="Python", text=None):
        container = Container(text, self.parent, self.desk)
        self.addTab(container, name)
        return container.edit

    def currentText(self, i=None):
        if not i:
            i = self.currentIndex()
        return self.widget(i).edit.selectedOrAll()
