import re

try:
    from PySide2.QtWidgets import *
    from PySide2.QtGui import *
    from PySide2.QtCore import *
except ImportError:
    from PySide.QtGui import *
    from PySide.QtCore import *

from ..syntax import pythonSyntax
from .autoCompleteWidget import AutoCompleteWidget
import jedi


class InputWidget(QTextEdit):
    def __init__(self, parent, desk=None):
        super(InputWidget, self).__init__(parent)
        self.parent = parent
        self.desk = desk
        # Auto wrap
        self.setWordWrapMode(QTextOption.NoWrap)
        self.fontSize = 16
        self.comp = AutoCompleteWidget(parent, self)
        self.applyHightLighter()

    def wheelEvent(self, event):
        if event.modifiers() == Qt.ControlModifier:
            if event.delta() > 0:
                self.changeFontSize(True)
            else:
                self.changeFontSize(False)
        QTextEdit.wheelEvent(self, event)

    def changeFontSize(self, up):
        if up:
            self.fontSize = self.fontSize + 1
        else:
            self.fontSize = max(1, self.fontSize - 1)
        self.setTextEditFontSize(self.fontSize)

    def setTextEditFontSize(self, size):
        style = '''QTextEdit
        {
            font-size: %spx;
        }''' % size
        self.setStyleSheet(style)

    def applyHightLighter(self, theme=None, qss=None):
        pythonSyntax.PythonHighlighter(self.document())

    def selectedOrAll(self):
        script = self.textCursor().selection().toPlainText()
        if script:
            return script
        else:
            script = self.toPlainText()
            if script:
                return script
            else:
                return None

    def keyPressEvent(self, event):
        QTextEdit.keyPressEvent(self, event)
        self.autoComplete()

    def autoComplete(self):
        script = self.toPlainText()
        tc = self.textCursor()
        pos = tc.position()
        if re.match("[a-zA-Z0-9_.]", script[pos-1]) and pos:
            # print(script)
            bNum = tc.blockNumber() + 1
            cNum = tc.columnNumber()
            jScript = jedi.Script(script, bNum, cNum, "")
            # print(jScript.completions())
            try:
                self.comp.updateAutoCompleteList(jScript.completions())
            except:
                self.comp.updateAutoCompleteList()
        else:
            self.comp.updateAutoCompleteList()

    def moveCompleter(self):
        rec = self.cursorRect()
        pt = self.mapToGlobal(rec.bottomRight())
        x = 0
        y = 0
        if self.comp.isVisible() and self.desk:
            currentScreen = self.desk.screenGeometry(self.mapToGlobal(rec.bottomRight()))
            futureCompGeo = self.comp.geometry()
            futureCompGeo.moveTo(pt)
            if not currentScreen.contains(futureCompGeo):
                i = currentScreen.intersect(futureCompGeo)
                x = futureCompGeo.width() - i.width()
                y = futureCompGeo.height() + self.comp.lineHeight if(futureCompGeo.height() - i.height()) > 0 else 0

        pt = self.mapToGlobal(rec.bottomRight()) + QPoint(10 - x, -y)

        self.comp.move(pt)
