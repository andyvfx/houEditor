try:
    from PySide2.QtWidgets import *
    from PySide2.QtGui import *
    from PySide2.QtCore import *
except ImportError:
    from PySide.QtGui import *
    from PySide.QtCore import *


class HistoryWidget(QTextBrowser):
    def __init__(self):
        super(HistoryWidget, self).__init__()
        # Auto wrap
        self.setWordWrapMode(QTextOption.NoWrap)
        self.fontSize = 16
        # self.setTextEditFontSize(self.fontSize)

    def showMessage(self, msg):
        self.moveCursor(QTextCursor.End)
        cursor = self.textCursor()
        cursor.insertText(str(msg)+"\n")
        self.setTextCursor(cursor)
        self.moveCursor(QTextCursor.End)
        self.ensureCursorVisible()

    def wheelEvent(self, event):
        if event.modifiers() == Qt.ControlModifier:
            if event.delta() > 0:
                self.changeFontSize(True)
            else:
                self.changeFontSize(False)
        QTextBrowser.wheelEvent(self, event)

    def changeFontSize(self, up):
        if up:
            self.fontSize = self.fontSize + 1
        else:
            self.fontSize = max(1, self.fontSize - 1)
        self.setTextEditFontSize(self.fontSize)

    def setTextEditFontSize(self, size):
        style = '''QTextEdit
        {
            font-size: %spx;
        }''' % size
        self.setStyleSheet(style)
