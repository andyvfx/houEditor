try:
    from PySide2.QtWidgets import *
    from PySide2.QtGui import *
    from PySide2.QtCore import *
except ImportError:
    from PySide.QtGui import *
    from PySide.QtCore import *


class ScriptEditorDialog(object):
    def setupUI(self, scriptEditor):
        scriptEditor.setObjectName("scriptEditor")
        scriptEditor.resize(800, 609)

        # Create history&input layout
        self.centralWidget = QWidget(scriptEditor)
        self.centralWidget.setObjectName("centralWidget")
        self.frame = QFrame(self.centralWidget)
        # self.frame.setFrameShape(QFrame.NoFrame)
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Sunken)
        self.frame.setObjectName("frame")
        self.splitter = QSplitter(self.frame)
        self.splitter.setOrientation(Qt.Vertical)
        self.splitter.setObjectName("splitter")
        self.historyVerticalLayout = QVBoxLayout(self.frame)
        self.historyVerticalLayout.setContentsMargins(0, 0, 0, 0)
        self.historyVerticalLayout.setObjectName("historyVerticalLayout")
        self.inputVerticalLayout = QVBoxLayout(self.centralWidget)
        self.inputVerticalLayout.setSpacing(0)
        self.inputVerticalLayout.setContentsMargins(3, 3, 3, 3)
        self.inputVerticalLayout.setObjectName("inputVerticalLayout")
        self.historyVerticalWidget = QWidget(self.splitter)
        self.historyVerticalWidget.setObjectName("historyVerticalWidget")
        self.historyLayout = QVBoxLayout(self.historyVerticalWidget)
        self.historyLayout.setContentsMargins(0, 0, 0, 0)
        self.historyLayout.setObjectName("historyLayout")
        self.inputVerticalWidget = QWidget(self.splitter)
        self.inputVerticalWidget.setObjectName("inputVerticalWidget")
        self.inputLayout = QVBoxLayout(self.inputVerticalWidget)
        self.inputLayout.setContentsMargins(0, 0, 0, 0)
        self.inputLayout.setObjectName("inputLayout")
        self.historyVerticalLayout.addWidget(self.splitter)
        self.inputVerticalLayout.addWidget(self.frame)
        scriptEditor.setCentralWidget(self.centralWidget)

        # Create custom menubar
        self.menuBar = QMenuBar(scriptEditor)
        self.menuBar.setGeometry(QRect(0, 0, 800, 21))
        self.menuBar.setObjectName("menuBar")

        # Create file menu
        self.fileMenu = QMenu(self.menuBar)
        self.fileMenu.setTearOffEnabled(True)
        self.fileMenu.setObjectName("fileMenu")
        self.newAction = QAction(scriptEditor)
        self.newAction.setObjectName("newAction")
        self.npbAction = QAction(scriptEditor)
        self.npbAction.setObjectName("npbAction")
        self.fileMenu.addAction(self.newAction)
        self.fileMenu.addAction(self.npbAction)

        # Create edit menu
        self.editMenu = QMenu(self.menuBar)
        self.editMenu.setTearOffEnabled(True)
        self.editMenu.setObjectName("editMenu")
        self.undoAction = QAction(scriptEditor)
        self.undoAction.setObjectName("undoAction")
        self.editMenu.addAction(self.undoAction)

        # Create history menu
        self.historyMenu = QMenu(self.menuBar)
        self.historyMenu.setTearOffEnabled(True)
        self.historyMenu.setObjectName("historyMenu")

        # Create command menu
        self.commandMenu = QMenu(self.menuBar)
        self.commandMenu.setTearOffEnabled(True)
        self.commandMenu.setObjectName("commandMenu")
        self.executeAction = QAction(scriptEditor)
        self.executeAction.setObjectName("executeAction")
        self.commandMenu.addAction(self.executeAction)

        # Create view menu
        self.viewMenu = QMenu(self.menuBar)
        self.viewMenu.setTearOffEnabled(True)
        self.viewMenu.setObjectName("viewMenu")

        # Create help menu
        self.helpMenu = QMenu(self.menuBar)
        self.helpMenu.setTearOffEnabled(True)
        self.helpMenu.setObjectName("helpMenu")

        scriptEditor.setMenuBar(self.menuBar)
        self.menuBar.addAction(self.fileMenu.menuAction())
        self.menuBar.addAction(self.editMenu.menuAction())
        self.menuBar.addAction(self.historyMenu.menuAction())
        self.menuBar.addAction(self.commandMenu.menuAction())
        self.menuBar.addAction(self.viewMenu.menuAction())
        self.menuBar.addAction(self.helpMenu.menuAction())

        self.translateUI(scriptEditor)

    def translateUI(self, scriptEditor):
        scriptEditor.setWindowTitle(QApplication.translate("scriptEditor", "MainWindow", None))
        self.fileMenu.setTitle(QApplication.translate("scriptEditor", "File", None))
        self.newAction.setText(QApplication.translate("scriptEditor", "New...", None))
        self.npbAction.setText(QApplication.translate("scriptEditor", "New Python Buffer", None))
        self.editMenu.setTitle(QApplication.translate("scriptEditor", "Edit", None))
        self.undoAction.setText(QApplication.translate("scriptEditor", "Undo", None))
        self.historyMenu.setTitle(QApplication.translate("scriptEditor", "History", None))
        self.commandMenu.setTitle(QApplication.translate("scriptEditor", "Command", None))
        self.executeAction.setText(QApplication.translate("scriptEditor", "Execute", None))
        self.viewMenu.setTitle(QApplication.translate("scriptEditor", "View", None))
        self.helpMenu.setTitle(QApplication.translate("scriptEditor", "Help", None))
