try:
    from PySide2.QtWidgets import *
    from PySide2.QtGui import *
    from PySide2.QtCore import *
except ImportError:
    from PySide.QtGui import *
    from PySide.QtCore import *

from ..qss import qss


class AutoCompleteWidget(QListWidget):
    def __init__(self, parent=None, editor=None):
        super(AutoCompleteWidget, self).__init__(parent)
        self.setAlternatingRowColors(1)
        self.lineHeight = 16
        self.editor = editor
        self.setStyleSheet(qss)
        self.setAttribute(Qt.WA_ShowWithoutActivating)
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.Window)

    def updateAutoCompleteList(self, lines=None):
        self.clear()
        self.showAutoCompleteList()
        if lines:
            for x in lines:
                item = QListWidgetItem(x.name)
                item.setData(32, x)
                self.addItem(item)
            font = QFont("Consolas", self.lineHeight, False)
            fm = QFontMetrics(font)
            width = fm.width(" ") * max([len(x.name) for x in lines]) + 40
            self.resize(max(255, width), 255)
            self.setCurrentRow(0)
        else:
            self.hideAutoCompleteList()

    def showAutoCompleteList(self):
        self.show()
        self.raise_()
        self.editor.moveCompleter()

    def hideAutoCompleteList(self):
        self.hide()

if __name__ == "__main__":
    '''
    Unit Test
    '''
