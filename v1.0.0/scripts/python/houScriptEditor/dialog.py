import sys
import traceback

try:
    from PySide2.QtWidgets import *
    from PySide2.QtGui import *
    from PySide2.QtCore import *
except ImportError:
    from PySide.QtGui import *
    from PySide.QtCore import *

from .widgets.historyWidget import HistoryWidget
from .widgets.tabWidget import TabWidget
from .widgets.dialog import ScriptEditorDialog
from .qss import qss


class ScriptEditor(QMainWindow, ScriptEditorDialog):
    def __init__(self, parent=None):
        # QMainWindow.__init__(self, parent)
        super(ScriptEditor, self).__init__(parent)
        self.namespace = __import__("__main__").__dict__
        self.setStyleSheet(qss)
        self.initUI()

    def initUI(self):
        self.setupUI(self)
        self.history = HistoryWidget()
        self.historyLayout.addWidget(self.history)
        self.tab = TabWidget()
        self.inputLayout.addWidget(self.tab)

        # Set action shortcut
        # self.newAction.triggered.connect()
        self.newAction.setShortcut("Ctrl+N")
        # self.newAction.setShortcutContext(Qt.WidgetShortcut)
        self.npbAction.setShortcut("Ctrl+Shift+N")
        # self.npbAction.setShortcutContext(Qt.WidgetShortcut)
        self.undoAction.setShortcut("Ctrl+Z")
        # self.undoAction.setShortcutContext(Qt.WidgetShortcut)
        self.executeAction.triggered.connect(self.executeSelectedOrAll)
        self.executeAction.setShortcut("Ctrl+E")
        self.history.showMessage("file -f -new;")
        self.tab.addNewTab()
        self.tab.addNewTab(name="VEX")
        self.tab.widget(0).edit.setFocus()

    def executeSelectedOrAll(self):
        command = self.tab.currentText().strip()
        self.history.showMessage(command)
        # print(command)
        # print(type(command))
        self.executeCommand(str(command))

    def executeCommand(self, command=None):
        if command:
            tmp_stdout = sys.stdout

            sys.stdout = stdoutProxy(self.history.showMessage)
            try:
                try:
                    result = eval(command, self.namespace, self.namespace)
                    if result:
                        self.history.showMessage(repr(result))

                except SyntaxError:
                    exec command in self.namespace
            except SystemExit:
                self.close()
            except:
                traceback_lines = traceback.format_exc().split('\n')
                for i in (3, 2, 1, -1):
                    traceback_lines.pop(i)
                self.history.showMessage('\n'.join(traceback_lines))
            sys.stdout = tmp_stdout


class stdoutProxy(object):
    def __init__(self, write_func):
        self.write_func = write_func
        self.skip = False

    def write(self, text):
        if not self.skip:
            stripped_text = text.rstrip('\n')
            self.write_func(stripped_text)
            QCoreApplication.processEvents()
        self.skip = not self.skip
