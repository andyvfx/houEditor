@ECHO off
SET ROOT=C:\Program Files\Side Effects Software\Houdini 16.0.705
SET SCRIPTEDITOR_PATH=F:\houEditor\v1.0.0

SET PATH=%PATH%;%ROOT%\bin
SET HOUDINI_PATH=%SCRIPTEDITOR_PATH%;^&

ECHO [CGRnDStudio] Info: Launch %ROOT%\bin\houdinifx.exe %*
houdinifx.exe %*
IF %ERRORLEVEL% NEQ 0 (
    ECHO [CGRnDStudio] Error: houdini error - %ERRORLEVEL% && EXIT /B %ERRORLEVEL%
) ELSE (
    ECHO [CGRnDStudio] Info: houdini Successfully && EXIT /B 0
)
